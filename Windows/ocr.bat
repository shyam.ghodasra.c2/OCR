@echo off
REM to see what happens
mkdir c:\tmp
echo from %1 to %2 >>C:\tmp\ocrtransform.log
 
copy /Y %1 "C:\TMP\%~n1%~x1"
echo target %~d2%~p2%~n2
REM call tesseract and redirect output to $TARGET
"C:\Program Files (x86)\Tesseract-OCR\tesseract.exe" "C:\tmp\%~n1%~x1" "%~d2%~p2%~n2" -l eng

exit